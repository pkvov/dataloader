
(defproject dataloader "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.6.1"]
                 [ring/ring-core "1.7.1"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-anti-forgery "1.3.0"]
                 [ring/ring-jetty-adapter "1.7.1"]
                 [ring/ring-core]
                 [mysql/mysql-connector-java "5.1.39"]
                 [selmer "1.10.7"]
                 [korma "0.4.3"]
                 [migratus "0.8.32"]
                 [jarohen/nomad "0.7.0"]
                 [org.clojure/data.csv "0.1.3"]
                 [clj-time "0.13.0"]
                 [http-kit "2.1.19"]
                 [commons-io/commons-io "2.8.0"]
                 [enlive "1.1.6"]
                 [incanter "1.5.7"]
                 [org.clojure/core.memoize "0.5.8"]
                 [criterium "0.4.4"]
                 [clojurewerkz/quartzite "2.1.0"]
                 [org.clojure/tools.cli "0.2.4"]
                 ;;[org.apache.commons/commons-daemon "1.0.15"]
                 [net.sourceforge.htmlunit/htmlunit "2.38.0"]
                 [org.jsoup/jsoup "1.13.1"]
                 ;; [clj-json "0.5.3"]
                 [clj-http "3.12.0"]
                 [cheshire "5.7.1"]
                 [com.taoensso/tufte "1.1.1"]
                 ]
  :main dataloader.core
  :plugins [[lein-ring "0.12.4"]
            [org.apache.maven.wagon/wagon-ssh-external "2.6"]
            [camechis/deploy-uberjar "0.3.0"]]
  :uberjar-name "dataloader-0.1.0-SNAPSHOT-standalone.jar"
  :uberjar-exclusions [;; #"org/bouncycastle"
                       ;; "org/apache/commons/daemon"
                       ]
  :repositories [["releases" "scp://wsbz@y2.vikevc.com:2020/home/wsbz/repo/releases"]
                 ["snapshots" "scp://wsbz@y2.vikevc.com:2020/home/wsbz/repo/snapshots"]]
  :ring {:init dataloader.handler/init
         :destroy dataloader.handler/destroy
         :handler dataloader.handler/app
         }
  :target-path "target/%s"
  :profiles {:dev {:jvm-opts ["-Dnomad.env=dev"]
                   :dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring/ring-mock "0.3.0"]
                                  [midje "1.6.3"]
                                  ]
                   }
             :test {:jvm-opts ["-Dnomad.env=test"]
                    }
             :prod {:jvm-opts ["-Dnomad.env=prod"]}
             :uberjar {:aot :all}}
  )
