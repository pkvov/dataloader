(ns dataloader.betting
  (:require [dataloader.logistic :as logistic]
            [dataloader.statics :as statics]
            [clj-time.core :as t]
            [clj-time.coerce :as coerce]
            [incanter.core :as incanter]
            [incanter.charts :as charts]
            [taoensso.tufte :as tufte :refer [defnp p profiled profile]])
  )

;;输入 判断入场的函数 计算凯利的函数，资金分配的函数
;;输出 计算收益率数据，画收益率曲线

(defn find-first [f coll]
  (first (drop-while (complement f) coll)))

(defn partition-between 
  "Splits coll into a lazy sequence of lists, with partition 
   boundaries between items where (f item1 item2) is true.
   (partition-between = '(1 2 2 3 4 4 4 5)) =>
   ((1 2) (2 3 4) (4) (4 5))"
  [f coll]
  (lazy-seq
   (when-let [s (seq coll)]
     (let [fst (first s)]
       (if-let [rest-seq (next s)]
         (if (f fst (first rest-seq))
           (cons (list fst) (partition-between f rest-seq))
           (let [rest-part (partition-between f rest-seq)]
             (cons (cons fst (first rest-part)) (rest rest-part))))
         (list (list fst)))))))

(defnp betting
  "根据训练数据，入场函数, 计算是否参与投注
  训练数据主要是statics/export-for-spass-inner中导出的数据, 包含以下几个字段
  入场函数，主要是根据概率差，计算得到是否参与投注
  判断是否猜对了的函数，根据该函数判断是否猜正确了
  "
  [training-data predict-data partition-between-fn guess-fn]
  (if-let [remote-data (logistic/PredictProbaWithFit training-data predict-data)]
    (let [data (map-indexed (fn [i item]
                              (let [{:keys [Div Date HomeTeam AwayTeam
                                            proba_home_0 proba_home_1 proba_home_3]} item
                                    predict-data-item (nth predict-data i)]
                                (assert (= (select-keys item [:HomeTeam :AwayTeam :Div])
                                           (select-keys predict-data-item [:HomeTeam :AwayTeam :Div])))
                                (assert (= (coerce/from-string (:Date item)) 
                                           (coerce/from-string (:Date predict-data-item))))
                                (let [abs-proba-diff (Math/abs (- proba_home_3 proba_home_0))
                                      data-item (merge item predict-data-item)
                                      data-item (assoc data-item :abs-proba-diff abs-proba-diff)
                                      ]
                                  data-item)
                                )) remote-data)
          ;; test (as-> data d
          ;;        (incanter/to-dataset d)
          ;;        (incanter/$ :abs-proba-diff d)
          ;;        (charts/histogram d)
          ;;        (incanter/view d))
          count-total (count data)
          partition-data (partition-between partition-between-fn data)
          betting-data (for [partition-item partition-data]
                         (let [guess-items (guess-fn partition-item)]
                           guess-items))
          ;; test2 (as-> betting-data d
          ;;         (flatten d)
          ;;         (incanter/to-dataset d)
          ;;         (incanter/$ :abs-proba-diff d)
          ;;         (charts/histogram d)
          ;;         (incanter/view d))
          guess-right-column (as-> betting-data d
                               (flatten d) 
                               (incanter/to-dataset d)
                               (incanter/$ :guess-right d)
                               )
          guess-right-count (count (filter #(= 1 %) guess-right-column))
          guess-wrong-count (count (filter #(= 0 %) guess-right-column))
          guess-right-percent (/ guess-right-count (+ guess-right-count guess-wrong-count))
          ]
      betting-data)))

(defnp earnning
  "计算收益
  根据投注数据、投注函数计算收益率"
  [betting-data betting-fn]
  (loop [betting-data# betting-data principal 1]
    (if-let [bet-items (first betting-data#)]
      (if (empty? bet-items)
        (recur (rest betting-data#) principal)
        (let [new-principal (betting-fn principal bet-items)]
          (recur (rest betting-data#) new-principal)))
      principal)))

(defnp earnning-seq
  "根据投数据，得到收益序列数据"
  [betting-data betting-fn]
  (for [bet-items betting-data]
    (betting-fn 1 bet-items)))

(tufte/add-basic-println-handler! {})

(defn test-betting [training-years predict-years]
  (let [training-data (statics/export-for-spass-inner training-years)
        predict-data (statics/export-for-spass-inner predict-years)
        partition-between-fn (fn [item1 item2]
                               (let [match-time1 (coerce/from-date (:MatchTime item1))
                                     match-time2 (coerce/from-date (:MatchTime item2))]
                                 (try
                                   (>= (t/in-hours
                                        (t/interval match-time1 match-time2))
                                       1)
                                   (catch Exception e
                                     (str "item1" item1 "item2" item2 "caught exception: " (.getMessage e))))))
        guess-fn (fn [data-items]
                   (let [guess-items
                         (for [data-item data-items
                               :let [{:keys [proba_home_0 proba_home_1 proba_home_3]} data-item
                                     diff (- proba_home_3 proba_home_0)
                                     ;; B365H (:B365H data-item)
                                     ;; B365D (:B365D data-item)
                                     ;; B365A (:B365A data-item)
                                     ;; killy-home (statics/killy proba_home_3 B365H)
                                     ;; killy-draw (statics/killy proba_home_1 B365D)
                                     ;; killy-away (statics/killy proba_home_0 B365A)
                                     ;; killy (if (pos? diff)
                                     ;;         killy-home
                                     ;;         killy-away)
                                     guess (if (> (Math/abs diff) 0.59)
                                             true
                                             false)
                                     ;;guess (pos? killy)
                                     guess-right (cond (not guess) -1
                                                       (and guess
                                                            (> diff 0)
                                                            (= (:HomePoint data-item) 3)) 1
                                                       (and guess
                                                            (< diff 0)
                                                            (= (:HomePoint data-item) 0)) 1
                                                       :else 0)
                                     ]
                               :when guess]
                           (assoc data-item :proba-diff diff :guess guess :guess-right guess-right)
                           )
                         sorted-guess-items (sort-by #(Math/abs (:proba-diff %)) > guess-items)
                         ]
                     (take 2 sorted-guess-items)))
        betting-data (betting training-data predict-data partition-between-fn guess-fn)
        principal-distribution-fn (fn [principal bet-items]
                                    (let [items-count (count bet-items)
                                          avg (/ principal items-count)]
                                      (repeat items-count avg)))
        calc-bet-earnings-fn (fn [bet-items principal-seq]
                               (map-indexed (fn [i bet-item]
                                              (let [B365H (:B365H bet-item)
                                                    B365D (:B365D bet-item)
                                                    B365A (:B365A bet-item)
                                                    {:keys [proba_home_0 proba_home_1 proba_home_3]} bet-item
                                                    principal (nth principal-seq i)
                                                    killy-home (statics/killy proba_home_3 B365H)
                                                    killy-draw (statics/killy proba_home_1 B365D)
                                                    killy-away (statics/killy proba_home_0 B365A)
                                                    odds (if (pos? (:proba-diff bet-item))
                                                           B365H
                                                           B365A)
                                                    killy (if (pos? (:proba-diff bet-item))
                                                            killy-home
                                                            killy-away)
                                                    new-e (if (pos? killy)  ;;凯利负表示概率太低不值得投
                                                            (if (= 1 (:guess-right bet-item))
                                                              (* principal (+ 1 (* killy (- odds  1))))
                                                              (* principal (- 1 killy)))
                                                            principal)
                                                    ]
                                                (assoc bet-item :killy killy :principal principal :new-principal new-e))) bet-items))
        betting-fn (fn [principal bet-items]
                     (if (empty? bet-items)
                       principal
                       (let [principal-seq (principal-distribution-fn principal bet-items)
                             bet-earnings (calc-bet-earnings-fn bet-items principal-seq)
                             earnings (for [item bet-earnings]
                                        (:new-principal item))
                             ]
                         (apply + earnings))))
        betting-fn2 (fn [principal bet-items]
                      (if (empty? bet-items)
                        principal
                        (let [principal-seq (principal-distribution-fn principal bet-items)
                              bet-earnings (calc-bet-earnings-fn bet-items principal-seq)
                              ]
                          bet-earnings)))
        ]
    ;;(earnning betting-data betting-fn)
    (earnning-seq betting-data betting-fn2)
    ))

(defn test_ []
  (profile
   {}
   (let [data-set (->> (test-betting [2010 2013] [2014])
                       (filter #(not= 1 %))
                       flatten
                       incanter/to-dataset)
         cols [:AwayPoint :AwayTeam :B365A :B365D :B365H :Date :Div :HomePoint :HomeTeam :MatchTime :aimpro_a :aimpro_d :aimpro_h :diff_dr :diff_gaa :diff_gfa :diff_ppg :diff_rfag :diff_rfwag :diff_wr :f_hfag :f_hfwag :guess :guess-right :hfag :killy :new-principal :odds-DA :odds-HA :odds-HD :principal :proba-diff :proba_home_0 :proba_home_1 :proba_home_3]
         data (incanter/reorder-columns data-set cols)
         ]
     (incanter/save data "2010~2013_2014.csv"))))





(defn gamble
  "投注，并输出收益率曲线"
  [training-years
   predict-years
   partition-between-fn
   guess-fn
   betting-fn
   ]
  (let [trainning-data (statics/export-for-spass-inner training-years)
        predict-data (statics/export-for-spass-inner predict-years)
        betting-data (betting trainning-data predict-data partition-between-fn guess-fn)
        earnning-data (earnning-seq betting-data betting-fn)]
    earnning-data))

(defn export-all-middle-data []
  (let [years (range 2000 2017)
        r (for [y (range 2000 2014)]
            (range y (+ y 4)))
        ]
    (doseq [x r]
      (let [trainning-data (statics/export-for-spass x)
            file-name (str "[" (first x) "-" (last x) "]" ".csv")]
        (incanter/save trainning-data (str "middle-data/" file-name))
        )
      )
    (doseq [x years]
      (let [predict-data (statics/export-for-spass (vector x))
            file-name (str x ".csv")]
        (incanter/save predict-data (str "middle-data/" file-name)))
      )))

(defn gamble-all
  []
  (let [years (range 2000 2017)
        r (for [y (range 2000 2014)]
            (range y (+ y 4)))
        partition-between-fn (fn [item1 item2]
                               (let [match-time1 (coerce/from-date (:MatchTime item1))
                                     match-time2 (coerce/from-date (:MatchTime item2))]
                                 (try
                                   (>= (t/in-hours
                                        (t/interval match-time1 match-time2))
                                       1)
                                   (catch Exception e
                                     (str "item1" item1 "item2" item2 "caught exception: " (.getMessage e))))))
        
        guess-fn (fn [data-items]
                   (let [guess-items
                         (for [data-item data-items
                               :let [{:keys [proba_home_0 proba_home_1 proba_home_3]} data-item
                                     diff (- proba_home_3 proba_home_0)
                                     guess (if (> (Math/abs diff) 0.59)
                                             true
                                             false)
                                     guess-right (cond (not guess) -1
                                                       (and guess
                                                            (> diff 0)
                                                            (= (:HomePoint data-item) 3)) 1
                                                       (and guess
                                                            (< diff 0)
                                                            (= (:HomePoint data-item) 0)) 1
                                                       :else 0)
                                     ]
                               :when guess]
                           (assoc data-item :proba-diff diff :guess guess :guess-right guess-right)
                           )
                         sorted-guess-items (sort-by #(Math/abs (:proba-diff %)) > guess-items)
                         ]
                     (take 1 sorted-guess-items)))
        principal-distribution-fn (fn [principal bet-items]
                                    (let [items-count (count bet-items)
                                          avg (/ principal items-count)]
                                      (repeat items-count avg)))
        calc-bet-earnings-fn (fn [bet-items principal-seq]
                               (map-indexed (fn [i bet-item]
                                              (let [B365H (:B365H bet-item)
                                                    B365D (:B365D bet-item)
                                                    B365A (:B365A bet-item)
                                                    {:keys [proba_home_0 proba_home_1 proba_home_3]} bet-item
                                                    principal (nth principal-seq i)
                                                    killy-home (statics/killy proba_home_3 B365H)
                                                    killy-draw (statics/killy proba_home_1 B365D)
                                                    killy-away (statics/killy proba_home_0 B365A)
                                                    odds (if (pos? (:proba-diff bet-item))
                                                           B365H
                                                           B365A)
                                                    killy (if (pos? (:proba-diff bet-item))
                                                            killy-home
                                                            killy-away)
                                                    ;; killy 0.3
                                                    killy (if (pos? killy)
                                                            killy
                                                            0.3)
                                                    new-e (if (pos? killy)  ;;凯利负表示概率太低不值得投
                                                            (if (= 1 (:guess-right bet-item))
                                                              (* principal (+ 1 (* killy (- odds  1))))
                                                              (* principal (- 1 killy)))
                                                            principal)
                                                    ]
                                                (assoc bet-item :killy killy :principal principal :new-principal new-e))) bet-items))
        betting-fn (fn [principal bet-items]
                     (if (empty? bet-items)
                       principal
                       (let [principal-seq (principal-distribution-fn principal bet-items)
                             bet-earnings (calc-bet-earnings-fn bet-items principal-seq)
                             earnings (for [item bet-earnings]
                                        (:new-principal item))
                             ]
                         {:Date (:Date (first bet-items))
                          :BetReturn (apply + earnings)
                          :bet-earnings bet-earnings
                          })))
        ]
    (for [t r
          y years
          :when (< (last t) y)
          ]
      {:training-years t
       :predict-year y
       :data (gamble t [y] partition-between-fn guess-fn betting-fn)
       })))

(defn save-gamble-all-betting []
  (let [gamble-seq (gamble-all)]
    (doseq  [gamble-data gamble-seq]
      (let [{:keys [training-years predict-year data]} gamble-data
            items (filter map? data)
            bet-earnings (flatten (for [item items]
                                           (:bet-earnings item)))
            file-name (str "[" (first training-years) " " (last training-years) "]"
                           "_" predict-year ".csv")
            data-set (incanter/to-dataset bet-earnings)
            cols [:AwayPoint :AwayTeam :B365A :B365D :B365H :Date :Div :HomePoint :HomeTeam :MatchTime :aimpro_a :aimpro_d :aimpro_h :diff_dr :diff_gaa :diff_gfa :diff_ppg :diff_rfag :diff_rfwag :diff_wr :f_hfag :f_hfwag :guess :guess-right :hfag :killy :new-principal :odds-DA :odds-HA :odds-HD :principal :proba-diff :proba_home_0 :proba_home_1 :proba_home_3 :abs-proba-diff]
            data (incanter/reorder-columns data-set cols)
            ]
        (incanter/save data file-name)))))

(defn view-gamble-all []
  (let [gamble-seq (gamble-all)
        chart (let [gamble-data (first gamble-seq)
                    training-years (:training-years gamble-data)
                    predict-year (:predict-year gamble-data)
                    label-txt (format "[%d %d]-%d" (first training-years) (last training-years) predict-year)
                    dates (for [item (:data gamble-data)
                                :when (not= 1 item)]
                            (coerce/to-long (:Date item)))
                    data (for [item (:data gamble-data)
                               :when (not= 1 item)]
                           (:BetReturn item))
                    v-data (map-indexed (fn [index item]
                                          (apply * (take (inc index) data))) data)

                    ]
                (charts/time-series-plot dates v-data :title "收益率曲线" :legend true :series-label (keyword label-txt)))
        ]
    (doseq [gamble-data (rest gamble-seq)]
      (let [training-years (:training-years gamble-data)
            predict-year (:predict-year gamble-data)
            label-txt (format "[%d %d]-%d" (first training-years) (last training-years) predict-year)
            dates (for [item (:data gamble-data)
                        :when (not= 1 item)]
                    (coerce/to-long (:Date item)))
            data (for [item (:data gamble-data)
                       :when (not= 1 item)]
                   (:BetReturn item))
            v-data (map-indexed (fn [index item]
                                  (apply * (take (inc index) data))) data)

            ]
        (charts/add-lines chart dates v-data :series-label (keyword label-txt))))
    (incanter/view chart)))

