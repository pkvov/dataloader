(ns dataloader.statics
  (:use [incanter.core]
        [incanter.stats]
        [incanter.mongodb]
        [incanter.datasets]
        [incanter.io]
        [incanter.charts]
        [somnium.congomongo :only (mongo! drop-database! databases with-mongo with-db make-connection set-connection!)]
        )
  (:require ;;[dataloader.db.core :as db]
   [dataloader.config :as config]
   [clj-time.coerce :as coerce]
   [clj-time.core :as t]
   [clj-time.format :as tf]
   [clojure.core.memoize :as memo]
   [clojure.java.io :as io]
   [clojure.string :as s]
   [clojure.data.csv :as csv]
   [net.cgrand.enlive-html :as html]
   [org.httpkit.client :as http]
   [dataloader.utils :refer [parse-int
                             parse-float
                             team-convert-map]]
   [taoensso.tufte :as tufte :refer [defnp p profiled profile]]
   )
  )

;; (def conn (make-connection (get-in (config/config) [:mongo-database :db])
;;                            :host (get-in (config/config) [:mongo-database :host])
;;                            :port (get-in (config/config) [:mongo-database :port])))

;; (defn persist
;;   "use #(persist % true) to dry-run the insert"
;;   [v & d]
;;   ;;mongodb cannot store '.' in fields
;;   ;;change '.' to unicode U+FF0E
;;   ;; (time (if d
;;   ;;         (println "insert to incanter dataset.")
;;   ;;         (insert-dataset :footballdata_matches (to-dataset v))))
;;   (if d
;;     (println "insert to incanter dataset.")
;;     (insert-dataset :footballdata_matches (to-dataset v)))
;;   true)

;; (defn init-mongo-conn []
;;   (mongo! :db "footballdata_matches"))

;; (defn load-all-data []
;;   ;;(set-connection! conn)
;;   (init-mongo-conn)
;;   (drop-database! :footballdata_matches)
;;   (pmap persist (db/extract-every db/fetch-every 1000)))

;; (defnp get-season-dataset-inner [match-year match-div]
;;   (let [year-start-date (coerce/to-date (t/date-time (if (vector? match-year)
;;                                                               (first match-year)
;;                                                               match-year
;;                                                               ) 8 1))
;;         year-end-date (coerce/to-date (t/date-time (if (vector? match-year)
;;                                                             (second match-year)
;;                                                             (+ 1 match-year)) 7 31))
;;         ]
;;     (with-data ($where {:Div match-div
;;                         :Date {:gt year-start-date :lt year-end-date}} (fetch-dataset :footballdata_matches))
;;       ($order [:Date] :asc)
;;       )))

(defnp get-season-dataset-fast [match-year match-div]
  (let [year-start-date (coerce/to-date (t/date-time (if (vector? match-year)
                                                       (first match-year)
                                                       match-year
                                                       ) 8 1))
        year-end-date (coerce/to-date (t/date-time (if (vector? match-year)
                                                     (second match-year)
                                                     (+ 1 match-year)) 7 31))
        ]
    (with-data (fetch-dataset :footballdata_matches :where {:Div match-div
                                                            :Date {:$gt year-start-date
                                                                   :$lt year-end-date}})
      ($order [:Date] :asc))))


(def get-season-dataset
  ;;(memo/ttl get-season-dataset-inner :ttl/threshold 100000)
  (memo/ttl get-season-dataset-fast :ttl/threshold 10000)
  )

(defn get-recent-dataset [match-dataset date]
  (with-data ($where {:Date {:lt date}} match-dataset)
    ($order [:Date] :desc)
    ))

(defn filter-team-dataset [match-dataset teams]
  (with-data ($where (fn [row]
                       (and (contains? teams (:HomeTeam row))
                            (contains? teams (:AwayTeam row)))
                       )  match-dataset)
    ($order [:Date] :desc)
    )
  )

(defn filter-home-away-team-dataset [match-dataset {:keys [HomeTeam AwayTeam]}]
  (with-data ($where (fn [row]
                       (and (= HomeTeam (:HomeTeam row))
                            (= AwayTeam (:AwayTeam row)))
                       )  match-dataset)
    ($order [:Date] :desc)
    )
  )

(defn- convert-to-team-rows [match-dataset]
  (let [rows (:rows match-dataset)]
    (for [row rows]
      [{:Team (:HomeTeam row)
        :Date (:Date row)
        :Points (case (:FTR row)
                  "H" 3
                  "D" 1
                  "A" 0
                  0)
        :Win (case (:FTR row)
               "H" 1
               0)
        :Draw (case (:FTR row)
                "D" 1
                0)
        :FTG (parse-int
              (:FTHG row))
        :FTLG (parse-int (:FTAG row))}
       {:Team (:AwayTeam row)
        :Date (:Date row)
        :Points (case (:FTR row)
                  "H" 0
                  "D" 1
                  "A" 3
                  0)
        :Win (case (:FTR row)
               "A" 1
               0)
        :Draw (case (:FTR row)
                "D" 1
                0)
        :FTG (parse-int (:FTAG row)) 
        :FTLG (parse-int (:FTHG row)) }
       ]
      )))

(defn get-team-dataset
  ([match-dataset]
   (-> match-dataset
       convert-to-team-rows
       flatten
       to-dataset)
   ))

(defn take-recent-team-dataset [team-dataset n]
  (let [grouped-dataset ($group-by :Team team-dataset)
        grouped-team-rows (for [[_ a-team-dataset] grouped-dataset]
                            (take n (:rows a-team-dataset))
                            )
        ;;_ (println "grouped-team-rows:" grouped-team-rows)
        team-rows (flatten grouped-team-rows)
        ]
    (to-dataset team-rows)))

(defn rollup-by-team [team-dataset]
  (with-data (reduce #($join [:Team :Team] %1 %2)
                     [(->> team-dataset
                           ($rollup :mean :Points :Team)
                           )
                      (->> team-dataset
                           ($rollup :mean :Win :Team)
                           )
                      (->> team-dataset
                           ($rollup :mean :Draw :Team)
                           )
                      (->> team-dataset
                           ($rollup :mean :FTG :Team)
                           )
                      (->> team-dataset
                           ($rollup :mean :FTLG :Team)
                           )
                      ])
    ($order [:Points :Win] :desc)
    ))

(defnp get-teams [match-year match-div]
  (let [season-dataset (get-season-dataset match-year match-div)
        season-home-teams (distinct ($ :HomeTeam season-dataset))
        season-away-teams (distinct ($ :AwayTeam season-dataset))
        ]
    (set (distinct (concat season-home-teams season-away-teams)))))

(defn- calc-impro-single- [a b c]
  (let [d (+ (/ 1 a) (/ 1 b) (/ 1 c))]
    [(/ (/ 1 a) d) (/ (/ 1 b) d) (/ (/ 1 c) d)]
    )
  )

(defn- calc-impro-single [a b c]
  (if (or (clojure.string/blank? a)
          (clojure.string/blank? b)
          (clojure.string/blank? c)
          )
    [0 0 0]
    (let [a (parse-float a)
          b (parse-float b)
          c (parse-float c)]
      (calc-impro-single- a b c))))

(defn- calc-impro-multi [row]
  (let [ma (matrix
            (for [k ["B365" "BS" "BW" "GB" "IW" "LB"
                     "PS" "SO" "SB" "SJ" "SY" "VC" "WH"]]
              (let [m (select-keys row [(-> k
                                            (str "H")
                                            keyword)
                                        (-> k
                                            (str "D")
                                            keyword)
                                        (-> k
                                            (str "A")
                                            keyword)])]
                (apply calc-impro-single (vals m)))
              ))]
    [(mean ($ 0 ma))
     (mean ($ 1 ma))
     (mean ($ 2 ma))
     ]))

(defn calc-odds-HD
  "通过欧式赔率，拟合双胜球的赔率
  主场胜平负(HDA) 的欧式赔率为a, b c
  "
  [a b c]
  (if-not (some nil? [a b c])
    (/ (+ (* a b) (* b c) (* a c)) (* 1.056 c (+ a b)))))

(defn calc-odds-DA
  "通过欧式赔率，拟合双胜球的赔率
  主场胜平负(HDA) 的欧式赔率为a, b c
  "
  [a b c]
  (if-not (some nil? [a b c])
    (/ (+ (* a b) (* b c) (* a c)) (* 1.0704 a (+ b c)))))

(defn calc-odds-HA
  "通过欧式赔率，拟合双胜球的赔率
  主场胜平负(HDA) 的欧式赔率为a, b c
  "
  [a b c]
  (if-not (some nil? [a b c])
    (/ (+ (* a b) (* b c) (* a c)) (* 1.056 b (+ a c)))))


(defn- calc-indicate [team indicate upper-teams down-teams last-season-statics-e0 last-season-statics-e1]
  (if (contains? upper-teams team)
    (let [avg-upper-indicate 
          (/ (apply + ($ indicate ($where (fn [row]
                                            (contains? upper-teams team))
                                          last-season-statics-e1)))
             (count upper-teams))
          avg-down-indicate
          (/ (apply + ($ indicate ($where (fn [row]
                                            (contains? down-teams team)
                                            )
                                          last-season-statics-e0)))
             (count down-teams))
          factor (/ avg-down-indicate avg-upper-indicate)
          ]
      (* factor (get (first (:rows ($where {:Team team} last-season-statics-e1))) indicate)))
    (get (first (:rows ($where {:Team team} last-season-statics-e0))) indicate)))

(defn- calc-diff-rfag [n y match-date home-team away-team]
  (let [last-n-matchs (-> (get-season-dataset y "E0")
                          (get-recent-dataset match-date)
                          get-team-dataset
                          (take-recent-team-dataset n)
                          )
        last-n-matchs-grouped ($group-by :Team last-n-matchs)
        
        home-team-last-n-match (get last-n-matchs-grouped {:Team home-team})
        away-team-last-n-match (get last-n-matchs-grouped {:Team away-team})
        h-n (count (:rows home-team-last-n-match))
        a-n (count (:rows away-team-last-n-match))
        diff-rfag (cond (and (> h-n 1)
                             (> a-n 1))
                        (- (/ (- (apply + ($ :FTG home-team-last-n-match))
                                 (apply + ($ :FTLG home-team-last-n-match)) ) h-n)
                           (/ (- (apply + ($ :FTG away-team-last-n-match))
                                 (apply + ($ :FTLG away-team-last-n-match))) a-n)
                           )
                        (and (= h-n 1)
                             (= a-n 1))
                        (- (- ($ :FTG home-team-last-n-match) ($ :FTLG home-team-last-n-match))
                           (- ($ :FTG away-team-last-n-match) ($ :FTLG away-team-last-n-match))
                           )
                        :else nil
                        )
        ]
    diff-rfag
    ))

(defn- calc-diff-rfwag [n y match-date home-team away-team]
  (let [last-n-matchs (-> (get-season-dataset y "E0")
                          (get-recent-dataset match-date)
                          get-team-dataset
                          (take-recent-team-dataset n)
                          )
        last-n-matchs-grouped ($group-by :Team last-n-matchs)
        home-team-last-n-match (get last-n-matchs-grouped {:Team home-team})
        away-team-last-n-match (get last-n-matchs-grouped {:Team away-team})
        h-n (count (:rows home-team-last-n-match))
        a-n (count (:rows away-team-last-n-match))
        diff-rfwag (cond (and (> h-n 1)
                              (> a-n 1))
                         (let [h-weight-matrix (mult (matrix (range h-n 0 -1)) (/ 1 (* 3 h-n)))
                               a-weight-matrix (mult (matrix (range a-n 0 -1)) (/ 1 (* 3 a-n)))
                               ]
                           (- (/ (- (apply + (mult h-weight-matrix ($ :FTG home-team-last-n-match)))
                                    (apply + (mult h-weight-matrix ($ :FTLG home-team-last-n-match)))
                                    ) h-n)
                              (/ (- (apply + (mult a-weight-matrix ($ :FTG away-team-last-n-match)))
                                    (apply + (mult a-weight-matrix ($ :FTLG away-team-last-n-match)))
                                    ) a-n)
                              )
                           )
                         (and (= h-n 1)
                              (= a-n 1))
                         (- (- ($ :FTG home-team-last-n-match) ($ :FTLG home-team-last-n-match))
                            (- ($ :FTG away-team-last-n-match) ($ :FTLG away-team-last-n-match))
                            )
                         :else nil
                         )
        ]
    diff-rfwag
    ))

(defn- calc-hfag [n y home-team away-team]
  (let [years-n (if (zero? (rem n 2))
                  (quot n 2)
                  (inc (quot n 2))
                  )
        last-n-matchs (-> (get-season-dataset [(- y years-n) y] "E0")
                          (filter-team-dataset #{home-team away-team})
                          get-team-dataset
                          (take-recent-team-dataset n)
                          )
        last-n-matchs-grouped ($group-by :Team last-n-matchs)

        home-team-last-n-match (get last-n-matchs-grouped {:Team home-team})
        away-team-last-n-match (get last-n-matchs-grouped {:Team away-team})
        h-n (count (:rows home-team-last-n-match))
        a-n (count (:rows away-team-last-n-match))
        hfag (cond (and (> h-n 1)
                        (> a-n 1))
                   (- (/ (- (apply + ($ :FTG home-team-last-n-match))
                            (apply + ($ :FTLG home-team-last-n-match)) ) h-n)
                      (/ (- (apply + ($ :FTG away-team-last-n-match))
                            (apply + ($ :FTLG away-team-last-n-match))) a-n)
                      )
                   (and (= h-n 1)
                        (= a-n 1))
                   (- (- ($ :FTG home-team-last-n-match) ($ :FTLG home-team-last-n-match))
                      (- ($ :FTG away-team-last-n-match) ($ :FTLG away-team-last-n-match))
                      )
                   :else nil
                   )
        ]
    hfag
    ))

(defn- calc-f-hfag [n y home-team away-team]
  (let [years-n (inc n)
        last-n-same-team-matchs
        (-> (get-season-dataset [(- y years-n) y] "E0")
            (filter-home-away-team-dataset {:HomeTeam home-team :AwayTeam away-team})
            get-team-dataset
            (take-recent-team-dataset n)
            )

        last-n-matchs-grouped ($group-by :Team last-n-same-team-matchs)
        
        home-team-last-n-match (get last-n-matchs-grouped {:Team home-team})
        away-team-last-n-match (get last-n-matchs-grouped {:Team away-team})
        
        h-n (count (:rows home-team-last-n-match))
        a-n (count (:rows away-team-last-n-match))
        f-hfag (cond (and (> h-n 1)
                          (> a-n 1))
                     (- (/ (- (apply + ($ :FTG home-team-last-n-match))
                              (apply + ($ :FTLG home-team-last-n-match)) ) h-n)
                        (/ (- (apply + ($ :FTG away-team-last-n-match))
                              (apply + ($ :FTLG away-team-last-n-match))) a-n)
                        )
                     (and (= h-n 1)
                          (= a-n 1))
                     (- (- ($ :FTG home-team-last-n-match) ($ :FTLG home-team-last-n-match))
                        (- ($ :FTG away-team-last-n-match) ($ :FTLG away-team-last-n-match))
                        )
                     :else nil
                     )]
    f-hfag
    ))

(defn- calc-f-hfwag [n y home-team away-team]
  (let [years-n (inc n)
        last-n-same-team-matchs
        (-> (get-season-dataset [(- y years-n) y] "E0")
            (filter-home-away-team-dataset {:HomeTeam home-team :AwayTeam away-team})
            get-team-dataset
            (take-recent-team-dataset n)
            )
        last-n-matchs-grouped ($group-by :Team last-n-same-team-matchs)
        home-team-last-n-match (get last-n-matchs-grouped {:Team home-team})
        away-team-last-n-match (get last-n-matchs-grouped {:Team away-team})
        
        h-n (count (:rows home-team-last-n-match))
        a-n (count (:rows away-team-last-n-match))
        f-hfwag (cond (and (> h-n 1)
                           (> a-n 1))
                      (let [h-weight-matrix (mult (matrix (range h-n 0 -1)) (/ 1 (* 3 h-n)))
                            a-weight-matrix (mult (matrix (range a-n 0 -1)) (/ 1 (* 3 a-n)))
                            ]
                        (- (/ (- (apply + (mult h-weight-matrix ($ :FTG home-team-last-n-match)))
                                 (apply + (mult h-weight-matrix ($ :FTLG home-team-last-n-match)))
                                 ) h-n)
                           (/ (- (apply + (mult a-weight-matrix ($ :FTG away-team-last-n-match)))
                                 (apply + (mult a-weight-matrix ($ :FTLG away-team-last-n-match)))
                                 ) a-n)
                           )
                        )
                      (and (= h-n 1)
                           (= a-n 1))
                      (- (- ($ :FTG home-team-last-n-match) ($ :FTLG home-team-last-n-match))
                         (- ($ :FTG away-team-last-n-match) ($ :FTLG away-team-last-n-match))
                         )
                      :else nil
                      )]
    f-hfwag))

(defnp export-for-spass-inner [years]
  (-> (for [y years]
        (let [season-dataset (get-season-dataset y "E0")
              _ (println "export-for-spass-inner 0")
              season-teams (get-teams y "E0")
              rows (:rows season-dataset)
              _ (println "export-for-spass-inner 1")
              last-season-statics-e0 (-> (get-season-dataset (dec y) "E0")
                                         get-team-dataset
                                         rollup-by-team
                                         )
              _ (println "export-for-spass-inner 2")
              last-season-statics-e1 (-> (get-season-dataset (dec y) "E1")
                                         get-team-dataset
                                         rollup-by-team
                                         )
              _ (println "export-for-spass-inner 3")
              last-season-teams-e0 (get-teams (dec y) "E0")
              _ (println "export-for-spass-inner 4")
              last-season-teams-e1 (get-teams (dec y) "E1")
              _ (println "export-for-spass-inner 5")
              upper-teams (clojure.set/difference season-teams (get-teams (dec y) "E0"))
              _ (println "export-for-spass-inner 6")
              down-teams (clojure.set/difference (get-teams (dec y) "E0") season-teams)
              _ (println "export-for-spass-inner 7")
              ]
          (for [row rows
                :let [home-team (:HomeTeam row)
                      away-team (:AwayTeam row)
                      match-date (:Date row)
                      _ (println "export-for-spass-inner 8")
                      diff-ppg (- (calc-indicate home-team :Points upper-teams down-teams last-season-statics-e0 last-season-statics-e1)
                                  (calc-indicate away-team :Points upper-teams down-teams last-season-statics-e0 last-season-statics-e1))
                      _ (println "export-for-spass-inner 9")
                      diff-wr (- (calc-indicate home-team :Win upper-teams down-teams last-season-statics-e0 last-season-statics-e1)
                                 (calc-indicate away-team :Win upper-teams down-teams last-season-statics-e0 last-season-statics-e1))
                      _ (println "export-for-spass-inner 10")
                      diff-dr (- (calc-indicate home-team :Draw upper-teams down-teams last-season-statics-e0 last-season-statics-e1)
                                 (calc-indicate away-team :Draw upper-teams down-teams last-season-statics-e0 last-season-statics-e1))
                      _ (println "export-for-spass-inner 11")
                      diff-gfa (- (calc-indicate home-team :FTG upper-teams down-teams last-season-statics-e0 last-season-statics-e1)
                                  (calc-indicate away-team :FTG upper-teams down-teams last-season-statics-e0 last-season-statics-e1))
                      diff-gaa (- (calc-indicate home-team :FTLG upper-teams down-teams last-season-statics-e0 last-season-statics-e1)
                                  (calc-indicate away-team :FTLG upper-teams down-teams last-season-statics-e0 last-season-statics-e1))
                      _ (println "export-for-spass-inner 12")
                      n 5
                      diff-rfag  (calc-diff-rfag n y match-date home-team away-team)
                      diff-rfwag (calc-diff-rfwag n y match-date home-team away-team)
                      _ (println "export-for-spass-inner 13")
                      n 3
                      hfag (calc-hfag n y home-team away-team)
                      n 4
                      f-hfag (calc-f-hfag n y home-team away-team)
                      f-hfwag (calc-f-hfwag n y home-team away-team)
                      _ (println "export-for-spass-inner 14")
                      [impro-home impro-draw impro-away]
                      (if (nil? (:BbAvH row))
                        (calc-impro-multi row)
                        (calc-impro-single (:BbAvH row) (:BbAvD row) (:BbAvA row)))
                      B365H (parse-float (:B365H row))
                      B365D (parse-float (:B365D row)) 
                      B365A (parse-float (:B365A row)) 
                      odds-HD (calc-odds-HD B365H B365D B365A)
                      odds-DA (calc-odds-DA B365H B365D B365A)
                      odds-HA (calc-odds-HA B365H B365D B365A)
                      _ (println "export-for-spass-inner 15")
                      ]
                :when (and (not (contains? upper-teams home-team))
                           (not (contains? upper-teams away-team))
                           (not (nil? diff-rfag))
                           (not (nil? diff-rfwag))
                           (not (nil? hfag))
                           (not (nil? f-hfag))
                           (not (nil? f-hfwag))
                           )]
            (array-map :HomeTeam home-team
                       :AwayTeam away-team
                       :Date (tf/unparse (tf/formatter "yyyy-MM-dd")
                                         (coerce/from-date (:Date row)))
                       :MatchTime (:MatchTime row)
                       :Div (:Div row)
                       :HomePoint (case (:FTR row)
                                    "H" 3
                                    "D" 1
                                    "A" 0
                                    0)
                       :AwayPoint (case (:FTR row)
                                    "H" 0
                                    "D" 1
                                    "A" 3
                                    0)
                       :diff_ppg (float diff-ppg) 
                       :diff_wr (float diff-wr) 
                       :diff_dr (float diff-dr) 
                       :diff_gfa (float diff-gfa) 
                       :diff_gaa (float diff-gaa) 
                       :diff_rfag (float diff-rfag) 
                       :diff_rfwag (float diff-rfwag) 
                       :hfag (float hfag) 
                       :f_hfag (float f-hfag) 
                       :f_hfwag (float f-hfwag) 
                       :aimpro_h (float impro-home) 
                       :aimpro_d (float impro-draw) 
                       :aimpro_a (float impro-away)
                       :B365H B365H
                       :B365D B365D
                       :B365A B365A
                       :odds-HD odds-HD
                       :odds-DA odds-DA
                       :odds-HA odds-HA
                       ))))
      flatten))

(defn export-for-spass [years]
  (to-dataset (export-for-spass-inner years))
  )

(defn calc-predict-by-params [intercept params values]
  (/ 1
     (+ 1 (Math/exp (* -1 (+ intercept (apply + (mult params values)))))))
  )

(defn calc-home-win-diff [params-map values]
  (let [p-away (calc-predict-by-params (get-in params-map [:away :intercept])
                                       (get-in params-map [:away :params])
                                       values)
        p-draw (calc-predict-by-params (get-in params-map [:draw :intercept])
                                       (get-in params-map [:draw :params])
                                       values)
        p-home (- 1 p-away p-draw)
        ]
    (if (pos? p-home)
      (- p-home p-away)
      )))

;; (defn killy [p t]
;;   (/ (- (* p t) 0.8) (- t 1))
;;   )

(defn killy [p t]
  (/ (- (* p t) 1) (- t 1)) 
  )


(defn predict-data
  "根据历史数据回归分析，猜中的比赛场次
  params-map spass 分析得出的系数
  match-years 一个vector 表示历史数据的年份 [2014 2016]
  diff-const 表示比赛概率差值需要大于一个常数，才猜测比赛
  每当利润达到最初始本金的x倍，则提取y%的资金沉淀到资金备用池
  recyle-times 可选参数，表示回收倍数X 默认10
  recyle-percent 可选参数, 表示提取的百分比y% 默认0.3"
  [params-map match-years diff-const &{:keys [recyle-times recyle-percent] :or {recyle-times 10 recyle-percent 0.3}}]
  (let [predict-data-set (export-for-spass match-years)
        rows (:rows predict-data-set)
        datas (let [e (atom 1)
                    e-recyle (atom 1)
                    e-recyle-remain (atom 0) 
                    ec (atom 1)   ;;组合玩法的收益率
                    ec-recyle (atom 1)
                    ec-recyle-remain (atom 0)
                    ]
                (for [row rows
                      :let [values (vals (select-keys row [:diff_ppg :diff_rfag :f_hfag :aimpro_h :aimpro_d]))]
                      :when (not-any? nil? values)]
                  (let [p-away (calc-predict-by-params (get-in params-map [:away :intercept])
                                                       (get-in params-map [:away :params])
                                                       values)
                        ;; p-draw (calc-predict-by-params (get-in params-map [:draw :intercept])
                        ;;                                (get-in params-map [:draw :params])
                        ;;                                values)
                        ;;p-home (- 1 p-away p-draw)
                        p-home (calc-predict-by-params (get-in params-map [:home :intercept])
                                                       (get-in params-map [:home :params])
                                                       values)
                        home-win-diff (- p-home p-away)
                        ;;home-win-diff (calc-home-win-diff params-map values)
                        guess (if (and (pos? p-home)
                                       (>= (Math/abs home-win-diff) diff-const))
                                true
                                false)
                        guess-right (cond (not guess)
                                          -1
                                          (and guess
                                               (= (:HomePoint row) 3)
                                               (> home-win-diff 0))
                                          1
                                          (and guess
                                               (= (:HomePoint row) 0)
                                               (< home-win-diff 0))
                                          1
                                          :else
                                          0)
                        guess-right-combin (cond (not guess)
                                                 -1
                                                 (and guess
                                                      (> home-win-diff 0)
                                                      (> (:HomePoint row) 0)
                                                      )
                                                 1
                                                 (and guess
                                                      (< home-win-diff 0)
                                                      (> (:AwayPoint row) 0)
                                                      )
                                                 1
                                                 :else
                                                 0)
                        B365H (:B365H row)
                        B365D (:B365D row)
                        B365A (:B365A row)
                        killy-home (killy p-home B365H)
                        killy-away (killy p-away B365A)

                        odds-HD (:odds-HD row)
                        odds-DA (:odds-DA row)
                        odds-HA (:odds-HA row)
                        ;; p-HD (/ 1 (* odds-HD 1.056))
                        ;; p-DA (/ 1 (* odds-DA 1.0704))
                        ;; killy-HD (killy p-HD odds-HD)
                        ;; killy-DA (killy p-DA odds-DA)
                        ]
                    (let [killy (if (pos? home-win-diff)
                                  killy-home
                                  killy-away
                                  )
                          odds (if (pos? home-win-diff)
                                 B365H
                                 B365A)
                          newe (cond (= 1 guess-right)
                                     (* @e (+ 1 (* killy (- odds 1))))
                                     (= 0 guess-right)
                                     (* @e (- 1 killy))
                                     :else
                                     @e)
                          
                          new-e-recyle (cond (= 1 guess-right)
                                             (* @e-recyle (+ 1 (* killy (- odds 1))))
                                             (= 0 guess-right)
                                             (* @e-recyle (- 1 killy))
                                             :else
                                             @e-recyle)
                          new-e-recyle (cond (> new-e-recyle recyle-times)
                                             (do
                                               (swap! e-recyle-remain +
                                                      (* new-e-recyle recyle-percent))
                                               (* new-e-recyle (- 1 recyle-percent)))
                                             :else
                                             new-e-recyle)

                          ;;组合玩法
                          ;; killy-combin (if (pos? home-win-diff)
                          ;;                killy-HD
                          ;;                killy-DA)
                          killy-combin killy
                          odds-combin (if (pos? home-win-diff)
                                        odds-HD
                                        odds-DA)
                          new-ec (cond (= 1 guess-right-combin)
                                       (* @ec (+ 1 (* killy-combin (- odds-combin 1))))
                                       (= 0 guess-right-combin)
                                       (* @ec (- 1 killy-combin))
                                       :else
                                       @ec)

                          new-ec-recyle (cond (= 1 guess-right-combin)
                                              (* @ec-recyle (+ 1 (* killy-combin
                                                                    (- odds-combin 1))))
                                              (= 0 guess-right-combin)
                                              (* @ec-recyle (- 1 killy-combin))
                                              :else
                                              @ec-recyle)
                          new-ec-recyle (cond (> new-ec-recyle recyle-times)
                                              (do
                                                (swap! ec-recyle-remain +
                                                       (* new-ec-recyle recyle-percent))
                                                (* new-ec-recyle (- 1 recyle-percent)))
                                              :else
                                              new-ec-recyle)
                          ]
                      (reset! e newe)
                      (reset! e-recyle new-e-recyle)
                      (reset! ec new-ec)
                      (reset! ec-recyle new-ec-recyle)
                      (assoc row
                             :guess guess
                             :guess_right guess-right
                             :guess_right_combin guess-right-combin
                             :p-home p-home
                             ;;:p-draw p-draw
                             :p-away p-away
                             ;;:p-HD p-HD
                             ;;:p-DA p-DA
                             :home-win-diff home-win-diff
                             :killy-home killy-home
                             ;;:killy-draw (killy p-draw (parse-float (:B365D row)))
                             :killy-away killy-away
                             ;;:killy-HD killy-HD
                             ;;:killy-DA killy-DA
                             :earn-ratio newe
                             :earn-ratio-recyle new-e-recyle
                             :earn-ratio-remain @e-recyle-remain
                             :earn-ratio-combin new-ec
                             :earn-ratio-recyle-combin new-ec-recyle
                             :earn-ratio-remain-combin @ec-recyle-remain
                             )))))
        ]
    (to-dataset datas)))

(defn reorder-predict-data [dataset]
  (reorder-columns dataset
                   [:Date
                    :Div
                    :HomePoint
                    :HomeTeam
                    :AwayPoint
                    :AwayTeam
                    :guess
                    :guess_right
                    :guess_right_combin
                    :earn-ratio
                    :earn-ratio-recyle
                    :earn-ratio-remain
                    :earn-ratio-combin
                    :earn-ratio-recyle-combin
                    :earn-ratio-remain-combin
                    :B365A
                    :B365D
                    :B365H
                    :odds-HD
                    :odds-DA
                    :odds-HA
                    :aimpro_a
                    :aimpro_d
                    :aimpro_h
                    :diff_dr
                    :diff_gaa
                    :diff_gfa
                    :diff_ppg
                    :diff_rfag
                    :diff_rfwag
                    :diff_wr
                    :f_hfag
                    :f_hfwag
                    :hfag
                    :home-win-diff
                    :killy-away
                    :killy-home
                    :killy-HD
                    :killy-DA
                    :p-away
                    :p-home
                    :p-HD
                    :p-DA
                    ]))

(defn predict-one [params-map y match-date diff-const home-team away-team odds-h odds-d odds-a]
  (let [last-season-statics-e0 (-> (get-season-dataset (dec y) "E0")
                                   get-team-dataset
                                   rollup-by-team
                                   )
        last-season-statics-e1 (-> (get-season-dataset (dec y) "E1")
                                   get-team-dataset
                                   rollup-by-team
                                   )
        season-teams (get-teams y "E0")
        upper-teams (clojure.set/difference season-teams (get-teams (dec y) "E0"))
        down-teams (clojure.set/difference (get-teams (dec y) "E0") season-teams)
        ]
    (if (or (contains? upper-teams home-team)
            (contains? upper-teams away-team))
      {:guess false
       :home-team home-team
       :away-team away-team
       :home-win-diff nil}
      (let [diff-ppg (- (calc-indicate home-team :Points upper-teams down-teams last-season-statics-e0 last-season-statics-e1)
                        (calc-indicate away-team :Points upper-teams down-teams last-season-statics-e0 last-season-statics-e1))
            
            n 5
            diff-rfag (calc-diff-rfag n y match-date home-team away-team)
            n 4
            f-hfag (calc-f-hfag n y home-team away-team)
            [impro-h impro-d impro-a] (calc-impro-single- odds-h odds-d odds-a)
            values [diff-ppg diff-rfag f-hfag impro-h impro-d]

            p-away (calc-predict-by-params (get-in params-map [:away :intercept])
                                           (get-in params-map [:away :params])
                                           values)
            ;; p-draw (calc-predict-by-params (get-in params-map [:draw :intercept])
            ;;                                (get-in params-map [:draw :params])
            ;;                                values)
            ;;p-home (- 1 p-away p-draw)
            p-home (calc-predict-by-params (get-in params-map [:home :intercept])
                                           (get-in params-map [:home :params])
                                           values)
            ;;home-win-diff (calc-home-win-diff params-map values)
            home-win-diff (- p-home p-away)
            guess (if (and (pos? p-home)
                           (>= (Math/abs home-win-diff) diff-const))
                    true
                    false)
            ]
        (if (some nil? values)
          {:guess false
           :home-team home-team
           :away-team away-team
           :home-win-diff nil}
          (array-map
           :guess guess
           :home-team home-team
           :away-team away-team
           :p-home p-home
           ;;:p-draw p-draw
           :p-away p-away
           :home-win-diff home-win-diff
           :killy-home (killy p-home odds-h)
           ;;:killy-draw (killy p-draw odds-d)
           :killy-away (killy p-away odds-a))
          )))))

(defn calc-earn-ratio [guess-data-set]
  (let [rows (:rows guess-data-set)]
    (loop [rows# rows e 1 last-date nil]
      (let [r (first rows#)]
        (if (nil? r)
          e
          (let [march-date (:Date r)
                home-win-diff (:home-win-diff r)
                killy-home (:killy-home r)
                killy-away (:killy-away r)
                B365H (:B365H r)
                B365A (:B365A r)
                killy (if (pos? home-win-diff)
                        killy-home
                        killy-away
                        )
                odds (if (pos? home-win-diff)
                       B365H
                       B365A)
                newe (if (= last-date march-date)
                       e  ;;相同日期的比赛只是猜第一场，由于投注时间限制
                       (if (= 1 (:guess_right r))
                         (* e (+ 1 (* killy (- odds  1))))
                         (* e (- 1 killy)))
                       )
                ]
            (recur (rest rows#) newe march-date))
          )))))

(defn predict-data-statics [predict-data-set]
  (let [guess-column ($ :guess predict-data-set)
        guess-right-column ($ :guess_right predict-data-set)
        guess-right-combin-column ($ :guess_right_combin predict-data-set)
        total-count (count ($ :Div predict-data-set))
        guess-count (count (filter #(= true %) guess-column))
        guess-right-count (count (filter #(= 1 %) guess-right-column))
        guess-right-combin-count (count (filter #(= 1 %) guess-right-combin-column))
        guess-wrong-count (- guess-count guess-right-count)
        guess-wrong-combin-count (- guess-count guess-right-combin-count)
        ;; guess-wrong-data-set ($where {:guess_right 0} predict-data-set)
        ;; earn-ratio-revert-column
        ;; (rest (reductions #(- 1 (/ %2 %1)) ($ :earn-ratio guess-wrong-data-set)))
        ;; earn-ratio-recyle-revert-column
        ;; (rest (reductions #(- 1 (/ %2 %1)) (plus ($ :earn-ratio-recyle guess-wrong-data-set)
        ;;                                          ($ :earn-ratio-remain guess-wrong-data-set))))
        ;; guess-wrong-combin-data-set ($where {:guess_right_combin 0} predict-data-set)
        ;; earn-ratio-combin-revert-column
        ;; (rest (reductions #(- 1 (/ %2 %1))
        ;;                   ($ :earn-ratio-combin guess-wrong-combin-data-set)))
        ;; earn-ratio-combin-recyle-revert-column
        ;; (rest (reductions #(- 1 (/ %2 %1)) (plus ($ :earn-ratio-recyle-combin guess-wrong-combin-data-set)
        ;;                                          ($ :earn-ratio-remain-combin guess-wrong-combin-data-set))))

        guess-data-set ($where {:guess true} predict-data-set)
        earn-ratio (calc-earn-ratio guess-data-set)
        ]
    (when (and (pos? total-count)
               (pos? guess-count))
      (array-map ;; :date1 date1
       ;; :date2 date2
       :count total-count
       :guess-count guess-count
       :guess-right-count guess-right-count
       :guess-right-combin-count guess-right-combin-count
       :guess-percent (float (/ guess-count total-count))
       :guess-right-percent (float (/ guess-right-count guess-count))
       :guess-right-combin-percent (float (/ guess-right-combin-count guess-count))
       
       :guess-wrong-count guess-wrong-count
       ;; :guess-wrong-max-revert (apply max earn-ratio-revert-column)
       ;; :guess-wrong-avg-revert (mean earn-ratio-revert-column)
       ;; :guess-wrong-recyle-max-revert (apply max earn-ratio-recyle-revert-column)
       ;; :fuwaa-wrong-recyle-avg-revert (mean earn-ratio-recyle-revert-column)
       
       :guess-wrong-combin-count guess-wrong-combin-count
       :earn-ratio earn-ratio
       ;; :guess-wrong-combin-max-revert (apply max earn-ratio-combin-revert-column)
       ;; :guess-wrong-combin-avg-revert (mean earn-ratio-combin-revert-column)
       ;; :guess-wrong-combin-recyle-max-revert (apply max earn-ratio-combin-recyle-revert-column)
       ;; :guess-wrong-combin-recyle-avg-revert (mean earn-ratio-combin-recyle-revert-column)
       ))))

(defn calc-year-2005 []
  (save (predict-data
         {:away {:intercept 0.228 :params [0.096 -0.068 -1.190 -0.138 -1.745]} 
          :draw {:intercept 0.077 :params [0.050 0.005 -0.564 -1.518 4.281]}}
         [2005] 0.4) "./predict/2005.csv"))


(defn export-all [start end step]
  (doseq [y (range start end)
          :let [r (range y (+ y step))
                file-name (str (first r) "-" (last r) ".csv")
                ]
          ]
    (println "try to export" file-name)
    (save (export-for-spass r) (str "./for-spass/" file-name))
    (println "export" file-name "finished")
    )
  )

;;todo 更换所有的参数系数
(def params-map-vector [{:away
                         {:intercept 0.47486264    
                          :params   [-0.05675976
                                     -0.02013128
                                     -0.07973254
                                     -8.26263638
                                     4.40923533 
                                     ]} 
                         :home
                         {:intercept 1.21253406 
                          :params   [0.36214864 
                                     0.01603599 
                                     0.00689589 
                                     1.27493612 
                                     -6.42277270
                                     ]}}
                        {:away
                         {:intercept 1.04303825  
                          :params   [-0.53171223 
                                     -0.00142835 
                                     -0.06269965 
                                     -10.52718167
                                     3.05310218  
                                     ]} 
                         :home
                         {:intercept 1.26848123 
                          :params   [0.03930237 
                                     0.07414295 
                                     -0.00358809
                                     1.74498352 
                                     -6.81288718
                                     ]}}
                        {:away
                         {:intercept -0.06971999
                          :params   [-0.57364407
                                     0.08741480 
                                     -0.09655641 
                                     -1.17463662 
                                     1.64610703 
                                     ]}
                         :home
                         {:intercept 0.22360938  
                          :params   [0.34433619  
                                     0.15711284 
                                     -0.02601567
                                     5.11759906 
                                     -2.52170059
                                     ]}}
                        {:away
                         {:intercept -0.01072080
                          :params   [-0.69704227
                                     0.09713683 
                                     -0.10362579 
                                     -2.02293546 
                                     2.04375012 
                                     ]}
                         :home
                         {:intercept 0.24846211  
                          :params   [0.30932884  
                                     0.15742796 
                                     0.00036199 
                                     2.46738214 
                                     -0.80668669
                                     ]}}
                        {:away
                         {:intercept -0.15770370
                          :params   [-0.79132674
                                     0.05284566 
                                     -0.08288632 
                                     -1.48924940 
                                     1.70328951 
                                     ]}
                         :home
                         {:intercept 0.67066077 
                          :params   [0.16357307 
                                     0.13690881 
                                     -0.01607544
                                     1.49671829 
                                     -1.75042263
                                     ]}}
                        {:away
                         {:intercept -4.17886457
                          :params   [-0.10918796
                                     0.11790403 
                                     -0.01634499 
                                     9.05787207  
                                     5.32533909 
                                     ]} 
                         :home
                         {:intercept 1.93464514 
                          :params   [0.12781098 
                                     0.08224941 
                                     0.01149505 
                                     -3.92872488
                                     -1.21644763
                                     ]}}
                        {:away
                         {:intercept -2.66743093
                          :params   [-0.17739206
                                     -0.01087113
                                     0.00205290  
                                     6.64417761  
                                     1.57824977 
                                     ]} 
                         :home
                         {:intercept -5.78825298
                          :params   [-0.35828986
                                     0.07960723 
                                     0.02615257 
                                     11.68258940
                                     6.62101131 
                                     ]}}
                        {:away
                         {:intercept -2.19832317
                          :params   [-0.33943883
                                     0.10214201 
                                     0.07091358 
                                     1.99841008 
                                     4.35319507 
                                     ]} 
                         :home
                         {:intercept 2.66744707 
                          :params   [0.25873691 
                                     0.00573442 
                                     -0.01737046
                                     -6.15312192
                                     -2.01741022
                                     ]}}
                        {:away
                         {:intercept -1.30700000
                          :params   [-0.36000000
                                     0.12500000 
                                     0.05900000  
                                     -0.28300000 
                                     3.89000000 
                                     ]} 
                         :home
                         {:intercept 2.43600000 
                          :params   [0.48300000 
                                     0.05400000 
                                     -0.05500000
                                     -5.63700000
                                     -1.42300000
                                     ]}}
                        {:away
                         {:intercept -0.98565252
                          :params   [0.17849322 
                                     0.17485661 
                                     0.00356192 
                                     -2.25243535
                                     4.85163564 
                                     ]}
                         :home
                         {:intercept 2.70570879 
                          :params   [0.72570155 
                                     0.06621893 
                                     -0.07296793
                                     -6.99264440
                                     -1.19689754
                                     ]}}
                        {:away
                         {:intercept -0.88498378
                          :params   [0.24090921 
                                     0.19843038 
                                     -0.00018397
                                     -2.22884245
                                     5.06288583 
                                     ]} 
                         :home
                         {:intercept 2.25390382 
                          :params   [0.59736058 
                                     0.12021289 
                                     -0.04682823
                                     -5.12043527
                                     -1.29931126
                                     ]}}
                        {:away
                         {:intercept -0.31258565
                          :params   [0.10228728 
                                     0.22007781 
                                     -0.02439571
                                     -3.52764360
                                     4.66248405 
                                     ]} 
                         :home
                         {:intercept 3.17330133 
                          :params   [0.06489576 
                                     0.12932669 
                                     -0.00857670
                                     -7.86662380
                                     -2.12028973
                                     ]}}
                        {:away
                         {:intercept 0.41385672 
                          :params   [0.01016149 
                                     0.10767932 
                                     -0.00382151
                                     -5.86377363
                                     3.84984247 
                                     ]} 
                         :home
                         {:intercept 3.80683584 
                          :params   [-0.26599899
                                     0.05585930 
                                     0.02197787 
                                     -9.80706970
                                     -2.90334707
                                     ]}}

                        ;; {:away
                        ;;  {:intercept 0.07132306 
                        ;;   :params   [-0.49096192
                        ;;              0.20654409 
                        ;;              0.01651481 
                        ;;              -3.33661010
                        ;;              3.14335860 
                        ;;              ]} 
                        ;;  :home
                        ;;  {:intercept 3.96171796 
                        ;;   :params   [-0.79596869
                        ;;              0.06100969 
                        ;;              0.06834324 
                        ;;              -8.09263466
                        ;;              -4.51719151
                        ;;              ]}}
                        ])

(defn predict-all-one-year-step
  ([]
   (predict-all-one-year-step false))
  ([export?]
   (let [years (range 2004 2017)]
     (for [params-map params-map-vector
           y years
           diff-const (range 0.3 0.7 0.1)
           :let [y-index (.indexOf years y)
                 paramas-map-index (.indexOf params-map-vector params-map)
                 paramas-year (format "%d-%d" (+ 2000 paramas-map-index) (+ 2003 paramas-map-index))
                 ;;diff-const (float (/ diff 10))
                 ]
           :when (<= paramas-map-index y-index)
           ]
       (let [data (predict-data params-map [y] diff-const)
             data-statics (predict-data-statics data)
             ]
         (when export? 
           (println "try to export internal predict data")
           (when-not (empty? (:rows data))
             (save (reorder-predict-data data) (format "./predict/(%s)_%d_%f.csv" paramas-year y diff-const)))
           (println "export internal predict data paramas-year:" paramas-year "year:" y "diff-const:" diff-const "finished")
           )
         (merge data-statics {:year y
                              :paramas-year paramas-year
                              :diff-const diff-const})
         )))))



(defn reorder-one-year-step [data]
  (reorder-columns data
                   [:paramas-year
                    :year
                    :diff-const
                    :count
                    :guess-count
                    :earn-ratio
                    :guess-percent
                    
                    :guess-right-count
                    :guess-wrong-count
                    :guess-right-percent

                    :guess-right-combin-count
                    :guess-wrong-combin-count
                    :guess-right-combin-percent
                    ]))

(defn predict-all []
  (save  (reorder-one-year-step (to-dataset (predict-all-one-year-step true))) "./predict/predict_all.csv"))

(defn predict-future [params-map diff-const]
  (let [url "http://odds.football-data.co.uk/football/england/premier-league/"
        data-table (html/select (html/html-resource (java.net.URL. url)) [:table.couponTable])
        y (t/year (t/today))
        match-date (coerce/to-date (t/today))
        rows (for [tr (html/select data-table [:tr])
                   :let [cls (get-in tr [:attrs :class])]
                   :when (and cls
                              (s/starts-with? cls "row"))
                   ]
               (for [td (html/select tr [:td])
                     :let [cls (get-in td [:attrs :class])]
                     :when (not (nil? cls))
                     ]
                 (s/trim (html/text td))
                 ))
        predict-data (for [row rows]
                       (let [[teamvs en-odds-h en-odds-d en-odds-a] row
                             team-pair (first (s/split teamvs #" \n\t\t\t\t"))
                             home-v-away (s/split team-pair #" v ")
                             home-team (team-convert-map (first home-v-away)) 
                             away-team (team-convert-map (second home-v-away)) 
                             odds-h (+ 1 (read-string en-odds-h))
                             odds-d (+ 1 (read-string en-odds-d))
                             odds-a (+ 1 (read-string en-odds-a))
                             ]
                         (predict-one params-map y match-date diff-const home-team away-team odds-h odds-d odds-a)
                         )
                       )
        ]
    predict-data
    )
  )

(defn export-predict-future []
  (let [datas (to-dataset
               (predict-future
                {:home {:intercept 0.974 :params 
                        [-0.611 0.079 0.524 0.845 -5.879]} 
                 :away {:intercept 3.290 :params 
                        [0.349 0.065 -0.551 -1.491 -9.901]}} 0.4))]
    (when-not (empty? (:rows datas))
      (save (reorder-columns
             datas
             ["away-team" "guess" "home-team" "home-win-diff" "killy-away"
              "killy-draw" "killy-home" "p-away" "p-draw" "p-home"])
            "./predict/predict_futures.csv"))))



