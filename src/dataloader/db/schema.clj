(ns dataloader.db.schema
  (:require [dataloader.config :as config]))

(def db-spec
  {:subprotocol "mysql"
   :subname (format "//%s:%d/%s?zeroDateTimeBehavior=convertToNull&useSSL=false"
                    (get-in (config/config) [:database :host])
                    (get-in (config/config) [:database :port])
                    (get-in (config/config) [:database :db])) 
   :delimiters (get-in (config/config) [:database :delimiters])
   :user (get-in (config/config) [:database :user]) 
   :password (get-in (config/config) [:database :password]) })
