(ns dataloader.db.migrate
  (:require [migratus.core :as m]
            [dataloader.config :as config :refer [config config-dir]]
            [dataloader.db.core :as db]
            [dataloader.db.schema :as schema]
            [dataloader.db.mongo :as mongo]
            [clojure.java.io :as io]))

(def m-config {:store                :database
               :migration-dir        "sql"
               ;;"migrations/sql"
               :init-script          (str "init." (:nomad/environment (config)) ".sql")
               ;;:migration-table-name "migrations"
               :db schema/db-spec})

(def m-init-config
  (assoc-in m-config [:db :subname]
            (format "//%s:%d?zeroDateTimeBehavior=convertToNull&useSSL=false"
                     (get-in (config) [:database :host])
                     (get-in (config) [:database :port]))))

(defn seed []
  (db/seed)
  ;;(mongo/seed)
  )

(defn refresh []
  (db/refresh)
  ;;(mongo/refresh)
  )

(defn setup-database []
  (let [lock-name (str (:nomad/environment (config)) ".lock")
        lock-file (java.io.File. (config-dir) lock-name)]
    (if (.exists lock-file)
      (do
        (println "apply pending migrations")
        (m/migrate m-config)
        ;;refresh data
        (refresh)
        )
      (do
        (println "initial database m-init-config" (assoc-in m-init-config [:db :password] "******"))
        (m/init m-init-config)
        (m/reset m-config)
        (seed)
        (with-open [wrtr (io/writer lock-file)]
          (.write wrtr "database init success"))
        ))
    )
  )
