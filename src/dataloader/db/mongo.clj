(ns dataloader.db.mongo
  (:use [incanter.core]
        [incanter.mongodb]
        [somnium.congomongo :only (mongo! drop-database! databases with-mongo with-db make-connection set-connection!)])
  (:require [dataloader.db.core :as db]
   [dataloader.config :as config])
  )


(def conn (make-connection (get-in (config/config) [:mongo-database :db])
                           :host (get-in (config/config) [:mongo-database :host])
                           :port (get-in (config/config) [:mongo-database :port])))

(defn persist
  "use #(persist % true) to dry-run the insert"
  [v & d]
  ;;mongodb cannot store '.' in fields
  ;;change '.' to unicode U+FF0E
  ;; (time (if d
  ;;         (println "insert to incanter dataset.")
  ;;         (insert-dataset :footballdata_matches (to-dataset v))))
  (if d
    (println "insert to incanter dataset.")
    (insert-dataset :footballdata_matches (to-dataset v)))
  true)

(defn init-mongo-conn []
  (set-connection! conn)
  (mongo! :db "footballdata_matches"))


(defn load-all-data []
  ;;(set-connection! conn)
  (init-mongo-conn)
  (drop-database! :footballdata_matches)
  (doall (pmap persist (db/extract-every db/fetch-every 1000))))

(defn seed []
  ;;load all data to mongo db
  (load-all-data)
  )

(defn refresh []
  ;; todo better load refresh data to mongo db
  ;;(load-all-data)
  (println "todo refresh data to mongo db")
  )

