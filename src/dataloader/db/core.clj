(ns dataloader.db.core
  (:use korma.core
        [korma.db :only (defdb transaction rollback is-rollback?)]
        [somnium.congomongo :only (mongo! drop-database! databases with-mongo with-db make-connection set-connection!)])
  (:require [dataloader.db.schema :as schema]
            [dataloader.utils :refer [parse-float parse-int cn-team-to-team]]
            [org.httpkit.client :as http]
            [clj-http.client :as client]
            [clojure.string :as s]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clj-time.core :as t]
            [clj-time.format :as tf]
            [clj-time.coerce  :as coerce]
            [net.cgrand.enlive-html :as html]
            [dataloader.spider.core :as spider :refer [get-league]]
            [somnium.congomongo :as congomongo]
            ))

(defdb db schema/db-spec)

(defentity dictionary)
(defentity footballdata_matches)

(defn process-football-data-notes [body]
  (println "process-football-data-notes")
  (insert dictionary
          (values (remove nil? (for [line (s/split-lines body)]
                                 (let [matches (s/split line #"\s*=\s*")
                                       [A B] matches]
                                   (if (and A B)
                                     {:symbol A :meaning B :source "football-data.co.uk"}
                                     )))))))

(defn add-dictionary-football-data []
  (http/get "http://www.football-data.co.uk/notes.txt" nil
            (fn [{:keys [status headers body error]}]
              (if (not (nil? error))
                (println "failed to call http://www.football-data.co.uk/notes.txt error:" error)
                (process-football-data-notes body)
                ))))

(defn escape-column [column]
  (-> column
      (clojure.string/replace "Avg>" "Avg_>")
      (clojure.string/replace "Avg<" "Avg_<")
      (clojure.string/replace "Max>" "Max_>")
      (clojure.string/replace "Max<" "Max_<")
      (clojure.string/replace "." "U+FF0E")
      ))


(defn process-one-matche-csv [url body force-update]
  (let [csv-data (csv/read-csv
                  ;;(io/reader (byte-array body))
                  body
                  )
        header (first csv-data)
        body-data (rest csv-data)
        _ (println "csv-data count" (count body-data))
        ;; columns (map #(str "`" (clojure.string/replace % "." "U+FF0E") "`") (filter not-empty header))
        columns (map #(str "`" (escape-column %) "`") (filter not-empty header))
        date-index (.indexOf columns "`Date`")
        home-team-index (.indexOf columns "`HomeTeam`")
        away-team-index (.indexOf columns "`AwayTeam`")
        ;; _ (println "columns:" columns)
        ;;_ (println "date-index:" date-index)
        rows (for [row body-data
                   :when (not-every? clojure.string/blank? row)]
               ;;convert date field
               (let [;;_ (println "rows:" row)
                     row (clojure.core/update row
                                              date-index
                                              #(-> "dd/MM/yy"
                                                   (tf/formatter (t/time-zone-for-id "Europe/London"))
                                                   (tf/parse %)
                                                   coerce/to-sql-date))
                     row (if (= (get row home-team-index) "Middlesboro")
                           (clojure.core/assoc row home-team-index "Middlesbrough")
                           row
                           )
                     row (if (= (get row away-team-index) "Middlesboro")
                           (clojure.core/assoc row away-team-index "Middlesbrough")
                           row
                           )
                     ]
                 (zipmap columns  row)))
        ]
    (try
      (if force-update
        (doseq [row rows]
          (let [match-date (get row "`Date`")
                home-team (get row "`HomeTeam`")
                away-team (get row "`AwayTeam`")
                ;; _ (println "home-team" home-team "away-team" away-team "match-date" match-date)
                matches (select footballdata_matches
                                (where {:HomeTeam home-team
                                        :AwayTeam away-team
                                        :Date match-date}))]
            (if (= 0 (count matches))
              (do
                (println "insert footballdata_matches" home-team away-team match-date)
                (insert footballdata_matches (values row)))
              
              (do
                (println "update footballdata_matches" home-team away-team match-date)
                (update footballdata_matches
                        (set-fields row)
                        (where {:HomeTeam home-team
                                :AwayTeam away-team
                                :Date match-date}))))))
        (insert footballdata_matches
                (values rows)))
      (catch Exception e
        (println "url:" url " insert exception:" e)
        ))))

(defn download-csv [url]

  )

(defn add-one-matche-csv [url force-update]
  (println "add-one-matche-csv" url force-update)
  (http/get url {:as :byte-array
                 :socket-timeout 1000
                 :connection-timeout 1000}
            (fn [{:keys [status headers body error]}]
              (if (not= status 200)
                (println "failed to call" url " error: " error " status:" status)
                (do
                  (process-one-matche-csv url (io/reader body)
                                          force-update)))))
  
  ;; (let [{:keys [body status error]} (client/get url {:as :byte-array
  ;;                                                    :throw-exceptions false
  ;;                                                    :socket-timeout 1000
  ;;                                                    :connection-timeout 1000})]
  ;;   (if (not= status 200)
  ;;     (println "failed to call" url " error: " error " status:" status)
  ;;     (process-one-matche-csv url body force-update)))

  ;; (let [tmp-file (java.io.File/createTempFile "football" ".csv")
  ;;       rbc (java.nio.channels.Channels/newChannel (.openStream (java.net.URL. url)))
  ;;       fos (java.io.FileOutputStream. tmp-file)]
  ;;   (try
  ;;     (-> fos
  ;;         .getChannel
  ;;         (.transferFrom rbc 0 java.lang.Long/MAX_VALUE))
  ;;     (with-open [rdr (clojure.java.io/reader tmp-file)]
  ;;       (process-one-matche-csv url rdr force-update))
  ;;     (finally
  ;;       (.close rbc)
  ;;       (.close fos))
  ;;     )
  ;;   )

  ;; (let [tmp-file (java.io.File/createTempFile "football" ".csv")]
  ;;   (try
  ;;     (org.apache.commons.io.FileUtils/copyURLToFile
  ;;      (java.net.URL. url)
  ;;      tmp-file
  ;;      1000
  ;;      1000)
  ;;     (println "downloaded file" tmp-file)
  ;;     (with-open [rdr (clojure.java.io/reader tmp-file)]
  ;;       (process-one-matche-csv url rdr force-update))
  ;;     (finally
  ;;       (.delete tmp-file)
  ;;       )
  ;;     ))
  ;; (with-open [rdr (clojure.java.io/reader (spider/get-one-matche-csv url))]
  ;;   (process-one-matche-csv url rdr force-update))

  
  )


(defn add-one-year-football-data-england [year force-update]
  (println "add-one-year-football-data-england" year force-update)
  (let [urls [(format "http://www.football-data.co.uk/mmz4281/%s/E0.csv" year)
              (format "http://www.football-data.co.uk/mmz4281/%s/E1.csv" year)
              (format "http://www.football-data.co.uk/mmz4281/%s/E2.csv" year)
              (format "http://www.football-data.co.uk/mmz4281/%s/E3.csv" year)
              ;; (format "http://www.football-data.co.uk/mmz4281/%s/EC.csv" year)
              ]]
    (doseq [url urls]
      (Thread/sleep 500)
      (println "add-one-year-football-data-england" url)
      (add-one-matche-csv url force-update))
    ))

(defn add-football-data-england
  ([start-year]
   (add-football-data-england start-year false))
  
  ([start-year force-update]
   (println "add-football-data-england" start-year force-update)
   (let [today (t/now)
         y (t/year today)
         m (t/month today)
         d (t/day today)
         years (range start-year (if (and (>= m 8)
                                          (>= d 15))
                                   (inc y)
                                   y) 1)
         _ (println "years" years)
         ]
     (doseq [x years]
       (let [yearstr (format "%02d%02d" (mod x 100) (mod (inc x) 100))]
         (add-one-year-football-data-england yearstr force-update))))) 
  )

(defn add-matches-football-data []
  (add-football-data-england 1993)
  ;;todo add other league football data
  )

;;avoid 405 error use htmlunit



;; (defn get-league []
;;   ;; url http://www.okooo.com/soccer/league/17/
;;   (let [url "https://www.okooo.com/soccer/league/17/"]
;;     @(http/get url {:as :byte-array
;;                     :user-agent "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
;;                     :headers {"Accept" "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
;;                               "referer" "https://www.okooo.com/soccer/league/17/"
;;                               "Connection" "keep-alive"
;;                               "accept-encoding" "gzip, deflate, br"
;;                               "authority" "www.okooo.com"
;;                               "Cookie" "acw_tc=2f624a3a16114717743687130e78555f485ee44e6025b70970fd2b140d3cee; PHPSESSID=7bb485055ec97a0f38ffa5f257f3b152250bbe51"
;;                               "Cache-Control" "no-cache"
;;                               "User-Agent" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
;;                               }}
;;               (fn [{:keys [status headers body error]}]
;;                 (if (not= status 200)
;;                   (println "failed to call" url "error:" error "status:" status)
;;                   (let [html-text (String. (byte-array body) "GBK")
;;                         league-div (html/select (html/html-resource (java.io.ByteArrayInputStream. (.getBytes html-text)))
;;                                                 [:div.LotteryList_Data :a])
;;                         ]
;;                     (for [item league-div
;;                           :let [link-text (html/text item)]
;;                           :when (re-matches #"英超 \d+\/\d+ 赛季" link-text)
;;                           ]
;;                       item)))))))

(defn get-league-href [league-link]
  (get-in league-link [:href]))

(defn get-league-text [league-link]
  (get-in league-link [:text]))

(defn get-max-round
  [league-link]
  (let [href (get-league-href league-link)
        url (str "https://www.okooo.com" href)
        {:keys [status headers body error] :as resp} @(http/get url {:as :byte-array})
        html-text (String. (byte-array body) "GBK")
        round-table (html/select (html/html-resource
                                  (java.io.ByteArrayInputStream. (.getBytes html-text)))
                                 [:table.SmallFont.linkblock :td.linkblock_select :a.OddsLink])
        max-round-link (first round-table)
        ]
    (parse-int (html/text max-round-link)) 
    ))

(defn year-from-league [league]
  (let [[y1 y2] (re-seq #"\d+" league) ;;"英超 98/99 赛季"
        y (parse-int y1)]
    (if (> y 80)
      (+ y 1900)
      (+ y 2000))))

(defn date-midnight [time]
  (let [y (t/year time)
        m (t/month time)
        d (t/day time)]
    (t/date-time y m d)))

(defn process-one-round [league-link round]
  (let [href (get-league-href league-link)
        league (get-league-text league-link)
        year (year-from-league league)
        url (str "http://www.okooo.com" href "1-1-" round)
        _ (println "process-one-round" url)
        ;; todo get html by htmlunit
        ;; {:keys [status headers body error] :as resp} @(http/get url {:as :byte-array
        ;;                                                              :socket-timeout 1000
        ;;                                                              :connection-timeout 1000})
        ;; html-text (String. (byte-array body) "GBK")
        ;; data-table-tds (html/select (html/html-resource
        ;;                              (java.io.ByteArrayInputStream. (.getBytes html-text)))
        ;;                             [:table#team_fight_table.ddtable :tr :td])
        data-table-tds (spider/get-one-round url)
        headers (take 7 data-table-tds)
        contents (drop 7 data-table-tds)
        rows (partition 9 contents)
        ]
    (doseq [row rows]
      (let [[match-time ;;08-20 22:00
             round home-team score away-team _ _ _ _ ] row
            en-home-team (cn-team-to-team (s/trim (html/text home-team)))
            en-away-team (cn-team-to-team (s/trim (html/text away-team)))
            day-hour-str (s/trim (html/text match-time))
            [month _ _ _] (map parse-int (re-seq #"\d+" day-hour-str))
            time-str (format "%d-%s" (if (< month 8)
                                       (inc year) ;;next year
                                       year)
                             day-hour-str)
            match-joda-time (-> "yyyy-MM-dd HH:mm"
                                (tf/formatter (t/time-zone-for-id "Asia/Shanghai"))
                                (tf/parse time-str))
            match-sql-time (coerce/to-sql-time match-joda-time)
            london-time (t/to-time-zone match-joda-time (t/time-zone-for-id "Europe/London"))
            ;;convert to london date (trunc the time part)
            london-date (t/with-time-at-start-of-day london-time)
            match-sql-date (coerce/to-sql-date london-date)
            ]
        (when (re-matches #"\d+-\d+" (s/trim (html/text score)))
          ;;update match-time
          (let [matches (select footballdata_matches
                                (where {:HomeTeam en-home-team
                                        :AwayTeam en-away-team
                                        :Date match-sql-date
                                        }))]
            (if (= 1 (count matches))
              (do
                (update footballdata_matches
                        (set-fields {:MatchTime match-sql-time})
                        (where {:HomeTeam en-home-team
                                :AwayTeam en-away-team
                                :Date match-sql-date
                                }))
                (print "."))
              (println "matches not found or too many count:" (count matches)
                       "where:" {:HomeTeam en-home-team
                                 :AwayTeam en-away-team
                                 :Date match-sql-date
                                 }))))))))


(defn process-one-league-link [league-link]
  (if-let [max-round (get-max-round league-link)]
    (doseq [round (range 1 (inc max-round))]
      (process-one-round league-link round))))

(defn add-matches-time-data []
  (doseq [league-link (get-league)]
    (process-one-league-link league-link)))

(defn seed []
  ;;initial db seed
  (println "seed db data from footballdata website")
  (add-dictionary-football-data)
  (println "add-dictionary-football-data finished")
  (add-matches-football-data)
  (println "add-dictionary-football-data finished")
  ;;add football match time
  ;; (add-matches-time-data)
  ;; (println "add football match time finished")
  )

(defn refresh []
  (let [y (t/year (t/now))
        m (t/month (t/now))
        d (t/day (t/now))
        start-year (if (and (>= m 8)
                            (>= d 15))
                     y
                     (dec y))
        league-link (first (get-league))]
    (add-football-data-england start-year true)
    (process-one-league-link league-link)
    ))

(defn fetch-every [limit-n offset-n]
  (-> (select* footballdata_matches)
      (limit limit-n)
      (order :Date :ASC)
      (offset offset-n)
      ))

(defn extract-every
  "q is a fn that represents a query. q is a fn that takes 2 args, one for limit
  (l) and one for offset (o).
  Returns a lazy seq that stops when there is no more results for q."
  ([q l] (extract-every q l 0))
  ([q l o]
   (when-let [ret (seq (select (q l o)))]
     (cons ret (lazy-seq (extract-every q l (+ o l))))
     )))
