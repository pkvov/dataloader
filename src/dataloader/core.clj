(ns dataloader.core
  (:require [dataloader.db.core :as db]
            [dataloader.db.migrate :as mg]
            [dataloader.betting :as betting]
            [clojure.tools.cli :refer [cli]]
            [ring.adapter.jetty]
            [ring.adapter.jetty]
            [dataloader.handler :refer [app init]]
            )
  )
  

(def state (atom {}))

(def app-specs [["-p" "--port" "服务监听的端口号" :default 3000 :parse-fn #(Integer. %)]
                ["-v" "--verbose" :flag true :default true]])

(defn start
  "开启服务"
  [& {:keys [port] :or {port 3000}}]
  (when-let [s (:server @state)]
    (.stop s))
  (init)
  (let [s (ring.adapter.jetty/run-jetty app {:port port :join? false})]
    (swap! state assoc :server s)
    (println "server started " port)
    ))

(defn stop
  "关闭服务"
  []
  (when-let [s (:server @state)]
    (.stop s)
    (swap! state dissoc :server)))


;; Daemon implementation
;; (defn -init [this ^DaemonContext context]
;;   (let [args (.getArguments context)]
;;     ;;call handle init
;;     (init)
;;     (swap! state assoc :args args)))

;; (defn -start [this]
;;   (let [[opts args banner] (apply cli (:args @state) app-specs)
;;         {port :port verbose :verbose} opts
;;         ]
;;     (start :port port)))

;; (defn -stop [this]
;;   (stop))

;; (defn -destroy [this]
;;   )


(defn -main
  "the main start point of dataloader"
  [& args]
  (let [[opts args banner] (apply cli args app-specs)
        {port :port verbose :verbose} opts]
    (start :port port))
  
  ;;(println "load all data to Incanter mongo finished")
  ;;(println "try to generate betting test data, press enter to go on")
  ;;(read-line)
  ;;(betting/test)
  ;;(println "export finished")
  )
