(ns dataloader.job
  (:require [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as t]
            [clojurewerkz.quartzite.jobs :as j]
            [clojurewerkz.quartzite.jobs :refer [defjob]]
            [clojurewerkz.quartzite.schedule.daily-interval
             :refer [schedule monday-through-friday
                     starting-daily-at time-of-day ending-daily-at
                     with-interval-in-minutes]]
            [dataloader.db.core :as db]
            [dataloader.db.mongo :as mongo]
            )
  
  
  
  )


(defjob FreshData
  [ctx]
  (doall (db/refresh))
  (doall (mongo/refresh))
  )

(defn start []
  (let [s   (-> (qs/initialize) qs/start)
        job  (j/build
              (j/of-type FreshData)
              (j/with-identity (j/key "jobs.load.footballdata")))
        trigger (t/build
                 (t/with-identity (t/key "triggers.load.footballdata"))
                 (t/start-now)
                 (t/with-schedule (schedule
                                   (with-interval-in-minutes 60)
                                   ;;(monday-through-friday)
                                   ;;(starting-daily-at (time-of-day 9 00 00))
                                   ;; (ending-daily-at (time-of-day 17 00 00))
                                   )))]
    (qs/schedule s job trigger)))

