(ns dataloader.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [selmer.parser :as selmer]
            [selmer.middleware :refer [wrap-error-page]]
            [dataloader.config :refer [config]]
            [dataloader.db.migrate :as mg]
            [dataloader.statics :as statics]
            [dataloader.db.mongo :as mongo]
            [dataloader.job :as job]
            [cheshire.core :as json]
            )
  (:gen-class)
  )

(defn init
  "初始化程序"
  []
  (println "try to load football data ...")
  (mg/setup-database)
  (println "try to load football data finished")
  (println "load all data to Incanter mongo ...")
  (mongo/init-mongo-conn)
  (mongo/refresh)
  (job/start)
  )

(defn destroy
  "程序终止，备份当前程序状态"
  []
  
  )

(defn parse-int [s]
   (Integer. (re-find  #"\d+" s )))

(defn handle-export-for-spass [{{:keys [start-year end-year] :as params} :params :as request}]
  (json/generate-string (statics/export-for-spass-inner
                         (range (parse-int start-year)
                                (parse-int end-year)
                                ))))

(defroutes app-routes
  (GET "/" [] "Welcome to Bole football API")
  (GET "/export-for-spass" request
       (handle-export-for-spass request))
  
  )

(def app
  (as-> app-routes $
    (if (= (:nomad/environment (config)) "dev")
      (wrap-error-page $)
      $)
    (wrap-defaults $ (if (= (:nomad/environment (config) "test"))
                       (assoc-in site-defaults [:security :anti-forgery] false)
                       site-defaults))
    )

)
