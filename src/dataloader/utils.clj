(ns dataloader.utils
  (:require [clojure.string :as s])
  )

(defn parse-int [s]
  (if-not (s/blank? s)
    (Integer/parseInt (re-find #"\A-?\d+" s))))

(defn parse-float [s]
  (if-not (nil? s)
    (Float/parseFloat s)))



(def team-convert-map {"Swansea City" "Swansea"
                       "Stoke City" "Stoke"
                       "West Ham" "West Ham"
                       "Hull City" "Hull"
                       "Sunderland" "Sunderland"
                       "Crystal Palace" "Crystal Palace"
                       "AFC Bournemouth" "Bournemouth"
                       "Arsenal" "Arsenal"
                       "Southampton" "Southampton"
                       "Leicester City" "Leicester"
                       "West Bromwich Albion" "West Brom"
                       "Manchester City" "Man City"
                       "Manchester United" "Man United"
                       "Liverpool" "Liverpool"
                       "Everton" "Everton"
                       "Tottenham Hotspur" "Tottenham"
                       "Burnley" "Burnley"
                       "Middlesbrough" "Middlesbrough"
                       "Watford" "Watford"
                       "Chelsea" "Chelsea"
                       })

(def cn-team-convert-map
  {
   "QPR"            "QPR"
   "伊普斯维奇"      "Ipswich"          
   "伯恩利"          "Burnley"          
   "伯恩茅斯"        "Bournemouth"      
   "伯明翰"          "Birmingham"       
   "切尔西"          "Chelsea"
   "利兹联"          "Leeds"            
   "利物浦"          "Liverpool"        
   "加的夫城"        "Cardiff"          
   "南安普敦"        "Southampton"      
   "博尔顿"          "Bolton"           
   "哈德斯菲尔德"    "Huddersfield"     
   "埃弗顿"          "Everton"          
   "奥德汉姆"        "Oldham"           
   "女王公园巡游者"   "QPR"              
   "富勒姆"          "Fulham"           
   "巴恩斯利"        "Barnsley"
   "布拉德福德"      "Bradford"         
   "布莱克本"        "Blackburn"        
   "布莱克浦"        "Blackpool"        
   "布莱顿"          "Brighton"
   "布赖顿"          "Brighton"
   "德比郡"          "Derby"            
   "托特纳姆热刺"    "Tottenham"
   "斯托克城"        "Stoke"            
   "斯文登"          "Swindon"
   "斯旺西"          "Swansea"          
   "斯温登"          "Swindon"          
   "曼城"            "Man City"
   "曼彻斯特城"      "Man City"         
   "曼彻斯特联"      "Man United"       
   "曼联"            "Man United"
   "朴次茅斯"        "Portsmouth"       
   "查尔顿"          "Charlton"         
   "桑德兰"          "Sunderland"       
   "水晶宫"          "Crystal Palace"   
   "沃特福德"        "Watford"
   "温布顿"          "Wimbledon"        
   "热刺"           "Tottenham"
   "狼队"            "Wolves"
   "班士利"          "Barnsley"
   "米尔顿凯恩斯"     "Wimbledon"
   "米德尔斯堡"      "Middlesbrough"    
   "纽卡斯尔"        "Newcastle"
   "纽卡斯尔联"      "Newcastle"        
   "维冈竞技"        "Wigan"            
   "维拉"            "Aston Villa"
   "考文垂"          "Coventry"
   "莱切城"          "Leicester"
   "莱切斯特城"      "Leicester"        
   "西布朗"         "West Brom"
   "西布罗姆维奇"    "West Brom"
   "西汉姆"          "West Ham"
   "西汉姆联"        "West Ham"
   "诺丁汉森林"      "Nott'm Forest"    
   "诺维奇"          "Norwich"          
   "谢周三"          "Sheffield Weds"
   "谢菲尔德星期三"   "Sheffield Weds"
   "谢菲尔德联"      "Sheffield United" 
   "谢菲联"          "Sheffield United"
   "赫尔城"          "Hull"             
   "阿斯顿维拉"      "Aston Villa"      
   "阿森纳"          "Arsenal"          
   "雷丁"            "Reading"
   })

(defn cn-team-to-team [cn-team]
  (if-let [res (get cn-team-convert-map cn-team)]
    res
    (println cn-team "not found.. try to add" cn-team "to cn-team-convert-map")))
