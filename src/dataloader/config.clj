(ns dataloader.config
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [nomad :refer [defconfig]]))

(defconfig config (io/resource "config.edn"))

(defn config-dir []
  (if-let [d (get-in (config) [:config :dir])]
    (if (.isDirectory (io/file d))
      (io/file d)
      (println "warnings: invalid config dir %s" d))
    ))
