(ns dataloader.logistic
  (:require [dataloader.statics :as statics]
            ;;[clj-json.core :as json]
            [cheshire.core :as json]
            [org.httpkit.client :as http]
            [taoensso.tufte :as tufte :refer [defnp p profiled profile]])
  )


(defn trainning-mode [years]
  (let [spass-data (statics/export-for-spass-inner years)
        data-json (json/generate-string spass-data)
        ]
    (let [options {:headers {"Content-Type" "application/json"
                             "Authorization" "Basic RW1XZWJBcGk6ODE1YzdjNzU5NjY3OTUxZWU0OTEwYmNiZDRkMTkxZGM="}
                   :body data-json}
          {:keys [status headers body error] :as resp}
          @(http/post "http://emapi.excelmatrix.com:83/api/v1/LogisticReression/fit"
                      options)]
      (json/parse-string body true))))


(defn PredictProba [years]
  (let [spass-data (statics/export-for-spass-inner years)
        data-json (json/generate-string spass-data)]
    (let [options {:headers {"Content-Type" "application/json"
                             "Authorization" "Basic RW1XZWJBcGk6ODE1YzdjNzU5NjY3OTUxZWU0OTEwYmNiZDRkMTkxZGM="}
                   :body data-json}
          {:keys [status headers body error] :as resp}
          @(http/post "http://emapi.excelmatrix.com:83/api/v1/LogisticReression/PredictProba"
                      options)]
      (json/parse-string body true))))

(defnp PredictProbaWithFit
  "根据训练数据，现有的比赛数据，获取胜平负的概率差"
  [training-data predict-data]
  (let [post-data {:FootballTrainDto training-data
                   :FootballDataDto predict-data}
        post-data-json (json/generate-string post-data)
        options {:headers {"Content-Type" "application/json"
                           "Authorization" "Basic RW1XZWJBcGk6ODE1YzdjNzU5NjY3OTUxZWU0OTEwYmNiZDRkMTkxZGM="}
                 :body post-data-json}
        {:keys [status headers body error] :as resp}
        @(http/post "http://emapi.excelmatrix.com:83/api/v1/Trident/PredictProbaWithFit"
                    options)
        ]
    (let [{:keys [Code Message Data]} (json/parse-string body true)]
      (if (= Code 1)
        Data))))


