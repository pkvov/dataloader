(ns dataloader.spider.core
  (:require [clj-time.format :as f]
            [clj-time.core :as t])
  (:import [org.jsoup Jsoup]
           [com.gargoylesoftware.htmlunit WebClient BrowserVersion]
           )
  (:gen-class)
  )

;; ------------------------------------------htmlunit---------------------------

(defn make-client
  ([]
   (make-client (.getNickname (BrowserVersion/getDefault))))
  ([version]
   (let [v (clojure.string/upper-case version)]
     (new WebClient
          (cond
            (= v "CHROME") BrowserVersion/INTERNET_EXPLORER
            (= v "IE") BrowserVersion/INTERNET_EXPLORER
            (= v "FF68") BrowserVersion/FIREFOX_68
            (= v "FF60") BrowserVersion/FIREFOX_60
            :else (throw (Exception. (str "Browser " version " is unknown, current IE, CHROME, FF60, FF68"))))))))

(defn get-page
  [client url]
  (.getPage client url))

(defn get-nodes-by-xpath
  [node xpath]
  (.getByXPath node xpath))

(defn get-first-node-by-xpath
  [node xpath]
  (.getFirstByXPath node xpath))

(defn get-node-anchors
  [node]
  (get-nodes-by-xpath node "//a"))

(defn get-nodes-anchors
  [nodes]
  (flatten (map #(get-node-anchors %) nodes))) 

(defn get-node-attributes
  [node]
  (let [attrs (.getAttributes node)
        length (.getLength attrs)
	items (map #(.item attrs %) (range 0 length))
	hash (reduce #(merge %1 {(keyword (.getName %2)) (.getValue %2)}) {} items)]
    hash))

;; ------------------------------------------htmlunit---------------------------

(def custom-formatter (f/formatter "yyyy-MM"))

(defn get-league-nodes [data-str]
  (let [client (make-client "FF60")
        ;;_ (.setUseInsecureSSL client true)
        page0 (get-page client "https://www.okooo.com/")
        _ (.waitForBackgroundJavaScript client 1000)
        page (get-page client "https://www.okooo.com/soccer/league/17/")
        _ (.waitForBackgroundJavaScript client 1000)
        nodes (get-nodes-by-xpath page "(//div[@class='LotteryList_Data'])[2]//a")]
    nodes))

(def get-league-nodes-memo (memoize get-league-nodes))

(defn get-league []
  (let [date-str (f/unparse custom-formatter (t/now))]
   (for [node (get-league-nodes-memo date-str)]
     {:text (.getTextContent node)
      :href (.getHrefAttribute node)} )))


(defn get-one-round [url]
  (let [client (make-client "FF60")
        ;; _ (.setUseInsecureSSL client true)
        page0 (get-page client "https://www.okooo.com/")
        _ (.waitForBackgroundJavaScript client 1000)
        page (get-page client url)
        _ (.waitForBackgroundJavaScript client 1000)
        nodes (get-nodes-by-xpath page "//table[@id='team_fight_table']//tr/td")]
    (for [node nodes]
      (.getTextContent node))
    )
  )

(defn get-one-matche-csv [url]
  (let [client (make-client "FF60")
        page0 (get-page client url)]
    (->> page0
         .getWebResponse
         .getContentAsStream
         )
    )
  )

