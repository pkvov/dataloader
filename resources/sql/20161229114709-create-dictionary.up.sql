create table `dictionary` (
        id integer auto_increment primary key,
        `source` varchar(100) not null comment '字典的来源',
        symbol varchar(100) not null comment '缩写',
        meaning varchar(255) comment '含义（英文）',
        meaning_chinese varchar(255) comment '含义（中文）'
        )engine=innodb comment='字典表';


                                    
